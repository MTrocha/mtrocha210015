#include <ncurses.h> 
#include <unistd.h>
#include <thread>
#include <vector>
#include <string>
#include <mutex> 
#include <cstdlib>
#include <errno.h>

using std::thread;
using std::vector;
using std::string;
using std::mutex;

enum Kierunek
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

struct Coord
{
public:
	int X;
	int Y;
};/**/

class Znak 
{
private:
	Kierunek znakKierunek;
	bool gora();
	bool dol();
	bool prawo();
	bool lewo();
	void zmienKierunek(Kierunek kierunek); 
public:
	void Ruch();
	vector<Coord> AllPoints; 
	int Min_X;		 // minimalna szerokość pola ruchu
	int Max_X;		 // maksymalna szerokość pola ruchu
	int Min_Y;		 // minimalna wysokość pola ruchu
	int Max_Y;		 // maksymalna wysokość pola ruchu
	Znak(int pozycja);
	char jaki;
};

bool Znak::gora()
{
	for (Coord point : AllPoints)
	{
		if (point.Y - 1 < Min_Y)
		{
			return false;
		}
	}
	return true;
}

bool Znak::dol()
{
	for (Coord point : AllPoints)
	{
		if(point.Y + 1 > Max_Y)
		{
		return false;
		}
	}
	return true;
}

bool Znak::prawo()
{
	for(Coord point : AllPoints)
	{
		if(point.X + 1 > Max_X)
		{
		return false;
		}
	}
	return true;
}

bool Znak::lewo()
{
	for(Coord point : AllPoints)
	{
		if(point.X - 1 <Min_Y)
		{
		return false;
		}
	}
	return true;
}
void Znak::zmienKierunek(Kierunek kierunek)
{
	Coord point = AllPoints.at(0);

	switch (kierunek)
	{
	case UP:
		point.Y--;
		break;
	case DOWN:
		point.Y++;
		break;
	case LEFT:
		point.X--;
		break;
	case RIGHT:
		point.X++;
		break;
	}

	AllPoints.emplace(AllPoints.cbegin(), point);
	AllPoints.erase(AllPoints.cend()-1);
	znakKierunek = kierunek;
}

Znak::Znak(int pozycja)
{
	AllPoints = vector<Coord>();
	//losowanie kierunku początkowego
	int random = ((std::rand()%4)+1);
	
	switch(random)
	{
	case 1:
	znakKierunek = RIGHT;
	break;

	case 2:
	znakKierunek = LEFT;
	break;

	case 3:
	znakKierunek = DOWN;
	break;

	case 4:
	znakKierunek = UP;
	break;
	}

	for (int i = 0; i < pozycja; i++)
	{
		Coord tempPoint{};
		tempPoint.X = i;
		tempPoint.Y = 1;
		AllPoints.push_back(tempPoint);
	}/**/
}

// wykonanie ruchu, nie wychodząc poza pole
void Znak::Ruch()
{
	switch (znakKierunek)
	{
	case UP:
		if (gora())
		{
			zmienKierunek(UP);
		}
		else
		{
			if (prawo())
			{
				zmienKierunek(RIGHT);
			}
			else
			{
				zmienKierunek(LEFT);
			}
		}
		break;

	case DOWN: 
		if (dol())
		{
			zmienKierunek(DOWN);
		}
		else
		{
			if (prawo())
			{
				zmienKierunek(RIGHT);
			}
			else
			{
				zmienKierunek(LEFT);
			}
		}
		break;

	case LEFT: 
		if (lewo())
		{
			zmienKierunek(LEFT);
		}
		else
		{
			if (gora())
			{
				zmienKierunek(UP);
			}
			else
			{
				zmienKierunek(DOWN);
			}
		}
		break;

	case RIGHT: 
		if (prawo())
		{
			zmienKierunek(RIGHT);
		}
		else
		{
			if (gora())
			{
				zmienKierunek(UP);
			}
			else
			{
				zmienKierunek(DOWN);
			}
		}
		break;
	default: ;
	}
}

// funkcja obrysowująca każde z okien
void draw_borders(WINDOW *screen) {
	int x, y, i;
	getmaxyx(screen, y, x);
	//zaznaczenie rogu jako "+"
	mvwprintw(screen, 0, 0, "+");
	mvwprintw(screen, y - 1, 0, "+"); 
	mvwprintw(screen, 0, x - 1, "+"); 
	mvwprintw(screen, y - 1, x - 1, "+");
	//wyznaczenie pionowych i poziomych krawędzi
	for (i = 1; i < (y - 1); i++) { 
		mvwprintw(screen, i, 0, "|");
		mvwprintw(screen, i, x - 1, "|"); 
	}
	for (i = 1; i < (x - 1); i++) { 
		mvwprintw(screen, 0, i, "-"); 
		mvwprintw(screen, y - 1, i, "-"); 
	}
}

Znak* narysujZnak(WINDOW *screen)
{
	int x, y;
        getmaxyx(screen, y, x); // pobranie wymiarów okna
	int pozycja = 1;

	Znak* znak = new Znak(pozycja);

	// ustawienie minimalnych i wakrymalnych wartości pola tak aby znaki nie wychodziły poza ramkę
	znak->Min_Y = 2;
	znak->Max_Y = (y-2);
	znak->Min_X = 2;
	znak->Max_X = (x-3);

	return znak;
}

void rysuj_pojedynczy_znak(WINDOW *screen, Znak* znak) 
{		
	for(Coord point : znak->AllPoints)
	{
		mvwprintw(screen, (point.Y) , (point.X+1), "1");	
	}
}
void rysuj_pojedynczy_znak2(WINDOW *screen, Znak* znak) 
{		
	for(Coord point : znak->AllPoints)
	{
		mvwprintw(screen, (point.Y) , (point.X+1), "2");	
	}
}
mutex mtx;

void ruchZnaku(WINDOW *screen, const char* screenNumber)
{
	Znak* znak =  narysujZnak(screen); 
	
	while(1) {
		mtx.lock();				//pozyskanie blokady dla wątku
		draw_borders(screen);			//narysowanie ramki
		clear();				//wyczyszczenie ekranu
		rysuj_pojedynczy_znak(screen, znak);	//narysowanie znaku
		wrefresh(screen); 			//odświeżenie okna
		mtx.unlock();				//odblokowanie wątku
		usleep(200000);				
		znak->Ruch();				// wywołanie funkcji Ruch
		}
	delwin(screen); 				//usunięcie okna
}

void ruchZnaku2(WINDOW *screen, const char* screenNumber)
{
	Znak* znak =  narysujZnak(screen); 
	
	while(1) {
		mtx.lock();				//pozyskanie blokady dla wątku
		draw_borders(screen);			//narysowanie ramki
		clear();				//wyczyszczenie ekranu
		rysuj_pojedynczy_znak2(screen, znak);	//narysowanie znaku
		wrefresh(screen); 			//odświeżenie okna
		mtx.unlock();				//odblokowanie wątku
		usleep(600000);				
		znak->Ruch();				// wywołanie funkcji Ruch
		}
	delwin(screen); 				//usunięcie okna
}
int main (int argc, char *argv[]) 
{
	int parent_x, parent_y; 
	
	initscr(); 
	noecho(); 
	curs_set(FALSE); // wyłączenie widoczności kursora
	getmaxyx(stdscr, parent_y, parent_x); // pobranie wymiarów okna


	WINDOW *part_one = newwin(parent_y/2, parent_x / 2, 0, 0);
	WINDOW *part_two = newwin(parent_y/2, parent_x / 2, 0, (parent_x/2));
 	
	//stworzenie i wystartowanie wątków
	thread znak_1(ruchZnaku, part_one, "1");
	
	thread znak_2(ruchZnaku2, part_two, "2");
	

	znak_1.join();
	znak_2.join();

	getch();
    return 0;
}
